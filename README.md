# Command-line application to manage inventory balances. #
* Enter "_I + **amount**_" to add amount items, ex "_I10_".
* Enter "_S + **amount**_" to sell amount items, ex. "_S10_".
* Enter "_L_" to display current amount of items. 


## Project setup
```
npm install
```

### Hot-reloads for development
```
npm dev
```

### Compiles and minifies for production
```
npm start
```

### Test
```
npm test
```