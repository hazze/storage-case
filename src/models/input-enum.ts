export enum InputType {
    Sell = 'S',
    Add = 'I',
    Inventory = 'L',
    Help = 'H'
}