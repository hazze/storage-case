import Readline from 'readline';
import { InputType } from './models/input-enum';
import InventoryComponent from './inventory';
import OutputComponent from './output';

export default class InputComponent {
    private readline: Readline.Interface;
    private inventory: InventoryComponent;
    private output: OutputComponent;
    private readonly inputCommandPrompt = '\n' + 'What would you like to do?' + '\n' + '> ';

    constructor(inventory: InventoryComponent, output: OutputComponent) {
        this.readline = Readline.createInterface({
            input: process.stdin,
            output: process.stdout,
            terminal: false
        });
        this.inventory = inventory;
        this.output = output;
    }

    public getInput(prompt: string = this.inputCommandPrompt): void {
        this.readline.question(prompt, (response: string) => {
            // Parses input for correct pattern.
            const regex = /^([SIL])(\d*)$/;
            const match = response.match(regex);
            if (match) {
                // Valid input
                const type = match[1];
                const amount = match.length == 3 ? parseInt(match[2]) : undefined;
                switch(type) {
                    case InputType.Add:
                        this.inventory.add(amount) ? this.output.added(amount) : this.output.error();
                        break;
                    case InputType.Sell:
                        this.inventory.sell(amount) ? this.output.sold(amount) : this.output.invalidValue();
                        break;
                    case InputType.Inventory:
                        // Gives 'unknown command' if 'L' was written with an amount.
                        if (amount) {
                            this.output.unknownCommand(response);
                            break;
                        }
                        this.output.current(this.inventory.inventory);
                        break;
                    default:
                        this.output.unknownCommand(response);
                        break;
                }
            }
            else {
                // Invalid input
                this.output.unknownCommand(response);
                this.getInput('');
                return;
            }

            // Ask for next command.
            this.getInput(this.inputCommandPrompt);
        });
    }
}