import InventoryComponent from './../inventory';

describe('inventory', () => {
    let inventory: InventoryComponent;

    beforeEach(() => {
        inventory = new InventoryComponent();
    })

    it('should have inital value of 0', () => {
        expect(inventory.inventory).toEqual(0);
    })

    it('should have inventory value of added amount', () => {
        const value = 100;
        inventory.add(value);
        expect(inventory.inventory).toEqual(value);
    })

    it('should have correct inventory value after adding and selling', () => {
        const add = 100;
        const remove = 24;
        inventory.add(add);
        inventory.sell(remove)
        expect(inventory.inventory).toEqual(add-remove);
    })

    it('should not allow selling more items than in stock', () => {
        const add = 10;
        const remove = 20;
        inventory.add(add);
        inventory.sell(remove);
        expect(inventory.inventory).toEqual(add);
    })

    it('should have correct inventory value after adding multiple times', () => {
        const add: number[] = [10, 21, 9, 241];
        const total = add.reduce((a, b) => a + b, 0)
        add.forEach(x => {
            inventory.add(x);
        });
        expect(inventory.inventory).toEqual(total);
    })

    it('should not allow negative values to be added to inventory', () => {
        const add = -5;
        inventory.add(add);
        expect(inventory.inventory).toEqual(0);
    })
});
