import InputComponent from './input';
import InventoryComponent from './inventory';
import OutputComponent from './output';

class App {
    private output: OutputComponent;
    private inventory: InventoryComponent;
    private input: InputComponent;
    
    constructor() {
        this.output = new OutputComponent();
        this.inventory = new InventoryComponent();
        this.input = new InputComponent(this.inventory, this.output);
        this.init();
    }

    private init(): void {
        this.output.instructions();
        this.input.getInput();
    }
}

export default new App();