export default class InventoryComponent {
    private _inventory: number;

    constructor() {
        this._inventory = 0;
    }

    public get inventory(): number {
        return this._inventory;
    }

    public add(value: number): boolean {
        if (this.validate(value)) {
            this._inventory += value;
            return true;
        }
        return false;
    }

    public sell(value: number): boolean {
        if (this.validate(value)) {
            if (this.inventory < value) {
                return false;
            }
            this._inventory -= value;
            return true;
        }
        return false;
    }

    private validate(amount: number): boolean {
        return (!amount || amount <= 0) ? false : true;
    }
}