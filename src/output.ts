export default class OutputComponent {
    
    private message(msg: string[]): void {
        msg.forEach(x => {
            console.log(x);
        })
    }

    public instructions(): void {
        const instructions: string[] = [
            '# To add stock, write: "I<amount>"         #',
            '# To sell stock, write: "S<amount>"        #',
            '# To display current inventory, write: "L" #',
        ];
        this.message(instructions);
    }

    public added(value: number): void {
        const msg: string[] = [`Added ${value} items to the inventory`];
        this.message(msg);
    }

    public sold(value: number): void {
        const msg: string[] = [`Sold ${value} items from the inventory`];
        this.message(msg);
    }

    public current(value: number): void {
        const msg: string[] = [`Current inventory: ${value}`]
        this.message(msg);
    }

    public invalidValue(): void {
        const msg: string[] = [`Value to sell can't be larger than the current inventory value.`, 'Please try again.'];
        this.message(msg);
    }

    public error(): void {
        const msg: string[] = ['Invalid value, please try again'];
        this.message(msg);
    }

    public unknownCommand(command = ''): void {
        const msg: string[] = [`Command ${command} could not be parsed!`, 'Please try again.'];
        this.message(msg);
    }
}